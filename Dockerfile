FROM alpine:3.13
LABEL maintainer="Kirill Shtrykov <kirill@shtrykov.com>"

# set envitonment variables
ENV PHP_FPM_LISTEN              /var/run/php7-fpm.sock
ENV PHP_FPM_USER                www-data
ENV PHP_FPM_GROUP               www-data
ENV PHP_FPM_LISTEN_MODE         0660
ENV PHP_MEMORY_LIMIT            512M
ENV PHP_MAX_UPLOAD              50M
ENV PHP_MAX_FILE_UPLOAD         200
ENV PHP_MAX_POST                100M
ENV PHP_DISPLAY_ERRORS          On
ENV PHP_DISPLAY_STARTUP_ERRORS  On
ENV PHP_ERROR_REPORTING         E_COMPILE_ERROR\|E_RECOVERABLE_ERROR\|E_ERROR\|E_CORE_ERROR
ENV PHP_CGI_FIX_PATHINFO        0
ENV TIMEZONE                    Europe/Minsk

# install packages
RUN apk add --update \
    git \
    nginx \
    php7-fpm \
    php7-mcrypt \
    php7-json \
    tzdata

# configure timezone
RUN cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime \
    && echo "${TIMEZONE}" > /etc/timezone

# add user
RUN adduser -D -g 'www' -G ${PHP_FPM_GROUP} ${PHP_FPM_USER} \
    && chown -R ${PHP_FPM_USER}:${PHP_FPM_GROUP} /var/lib/nginx

# modifying configuration file www.conf
RUN sed -ri "s|^listen\s*=.*$|listen = ${PHP_FPM_LISTEN}|g" /etc/php7/php-fpm.d/www.conf \
    && sed -i "s|;listen.owner\s*=\s*nobody|listen.owner = ${PHP_FPM_USER}|g" /etc/php7/php-fpm.d/www.conf \
    && sed -i "s|;listen.group\s*=\s*nobody|listen.group = ${PHP_FPM_GROUP}|g" /etc/php7/php-fpm.d/www.conf \
    && sed -i "s|;listen.mode\s*=\s*0660|listen.mode = ${PHP_FPM_LISTEN_MODE}|g" /etc/php7/php-fpm.d/www.conf \
    && sed -ri "s|^user\s*=.*$|user = ${PHP_FPM_USER}|g" /etc/php7/php-fpm.d/www.conf

# modifying configuration file php-fpm.conf
RUN sed -i "s|;listen.owner\s*=\s*nobody|listen.owner = ${PHP_FPM_USER}|g" /etc/php7/php-fpm.conf \
    && sed -i "s|;listen.group\s*=\s*nobody|listen.group = ${PHP_FPM_GROUP}|g" /etc/php7/php-fpm.conf \
    && sed -i "s|;listen.mode\s*=\s*0660|listen.mode = ${PHP_FPM_LISTEN_MODE}|g" /etc/php7/php-fpm.conf \
    && sed -i "s|user\s*=\s*nobody|user = ${PHP_FPM_USER}|g" /etc/php7/php-fpm.conf \
    && sed -i "s|group\s*=\s*nobody|group = ${PHP_FPM_GROUP}|g" /etc/php7/php-fpm.conf \
    && sed -i "s|;log_level\s*=\s*notice|log_level = notice|g" /etc/php7/php-fpm.conf #uncommenting line

# modifying configuration file php.ini
RUN sed -i "s|display_errors\s*=\s*Off|display_errors = ${PHP_DISPLAY_ERRORS}|i" /etc/php7/php.ini \
    && sed -i "s|display_startup_errors\s*=\s*Off|display_startup_errors = ${PHP_DISPLAY_STARTUP_ERRORS}|i" /etc/php7/php.ini \
    && sed -ri "s/^error_reporting\s*=.*$/error_reporting = ${PHP_ERROR_REPORTING}/g" /etc/php7/php.ini \
    && sed -i "s|;*memory_limit =.*|memory_limit = ${PHP_MEMORY_LIMIT}|i" /etc/php7/php.ini \
    && sed -i "s|;*upload_max_filesize =.*|upload_max_filesize = ${PHP_MAX_UPLOAD}|i" /etc/php7/php.ini \
    && sed -i "s|;*max_file_uploads =.*|max_file_uploads = ${PHP_MAX_FILE_UPLOAD}|i" /etc/php7/php.ini \
    && sed -i "s|;*post_max_size =.*|post_max_size = ${PHP_MAX_POST}|i" /etc/php7/php.ini \
    && sed -i "s|;*cgi.fix_pathinfo=.*|cgi.fix_pathinfo= ${PHP_CGI_FIX_PATHINFO}|i" /etc/php7/php.ini \
    && sed -i "s|;*date.timezone =.*|date.timezone = ${TIMEZONE}|i" /etc/php7/php.ini

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
        && ln -sf /dev/stderr /var/log/nginx/error.log

WORKDIR /var/www

# clone privatebin repository
RUN git clone https://github.com/PrivateBin/PrivateBin privatebin \
    && cp privatebin/cfg/conf.sample.php privatebin/cfg/conf.php

# create data direcotry
RUN mkdir privatebin/data

# set permissions on www root
RUN chown -R ${PHP_FPM_USER}:${PHP_FPM_GROUP} /var/www

# mount data directory
VOLUME /var/www/privatebin/data

# copy nginx configuration
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

STOPSIGNAL SIGTERM

CMD php-fpm7 && nginx
